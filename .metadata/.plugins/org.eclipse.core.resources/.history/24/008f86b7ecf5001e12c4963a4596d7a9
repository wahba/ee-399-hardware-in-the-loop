/*
 * barometer.cpp
 *
 *  Created on: Mar 19, 2024
 *      Author: Samuel
 */

#include <sensor/barometer.hpp>

void HIL_Barometer::update_truth(const simulator_truth_t& truth){
	_pressure_ref = truth.pressure;
}

void HIL_Barometer::get_sensor_event(sensor_res_t* res) {
	memset(res, 0, sizeof(sensor_res_t));
	res->type = BARO;
	res->pressure = _pressure;
	res->valid = _last_response + _min_delay_ms >= HAL_GetTick();
	if(res->valid)
		_last_response = HAL_GetTick();
}

void HIL_Barometer::tick_data(){
	/** TODO apply successive models
	 * 1. bulk (bias)
	 * 2. random walk
	 * 3. white noise
	 * 4. saturation
	 * 5. quantisation
	 */
	_pressure = _pressure_ref;
	bulk_model(_pressure, _bias);

	saturation_model(_pressure, _min_value, _max_value);
	quantisation_model(_pressure, _resolution);
}

