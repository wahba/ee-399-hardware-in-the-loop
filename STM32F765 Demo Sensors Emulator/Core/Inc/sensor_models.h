/*
 * sensor_models.h
 *
 *  Created on: Apr 10, 2024
 *      Author: Samuel
 */

#ifndef INC_SENSOR_SENSOR_MODELS_H_
#define INC_SENSOR_SENSOR_MODELS_H_

#include "sensors.h"

void bulk_model(float *reading, float *bias);

void random_walk_drift_model(float *reading, float* random_walk, float *sampling_rate);

void white_noise_drift_model(float *reading, float* white_noise_density, float *sampling_rate);

void saturation_model(float *reading, float *min_range, float *max_range);

void quantization_model(float *reading, float *resolution);

float white_noise();

#endif /* INC_SENSOR_SENSOR_MODELS_H_ */
