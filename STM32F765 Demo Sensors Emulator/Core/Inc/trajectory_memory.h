/*
 * trajectory_memory.h
 *
 *  Created on: May 27, 2024
 *      Author: Samuel
 */

#ifndef TRAJECTORY_MEMORY_H_
#define TRAJECTORY_MEMORY_H_

#include "stdint.h"

float get_pressure(void);

#endif /* TRAJECTORY_MEMORY_H_ */
