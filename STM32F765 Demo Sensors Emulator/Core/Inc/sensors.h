/*
 * sensors.h
 *
 *  Created on: May 27, 2024
 *      Author: Samuel
 */

#ifndef INC_SENSORS_H_
#define INC_SENSORS_H_

#include "stdint.h"

/** TYPES */
typedef uint16_t sensor_uuid_t;

typedef uint32_t sensor_channel_addr_t;

typedef enum sensors_channels_t {
	INVALID_CHANNEL = 0, I2C_CHANNEL = 1,
} sensors_channels_t;

/** BASE */
typedef struct hil_base_sensor {
	sensors_channels_t channel;
	sensor_channel_addr_t addr;
	sensor_uuid_t uuid;
	uint32_t delay_ms;
	float sampling_rate;

	void (*tick)(void*);
} hil_base_sensor;

const sensor_uuid_t get_sensor_uuid(uint32_t addr, sensors_channels_t channel);

/** BAROMETERS */

typedef struct hil_barometer_sensor_t {
	float pressure;

	float bias;
	float white_noise_density;
	float random_walk;
	float temperature_bias;

	float min_range;
	float max_range;
	float resolution;

	hil_base_sensor base;
} hil_sensors_barometer_t;

void hil_sensors_new_barometer(hil_sensors_barometer_t* baro, sensors_channels_t channel, sensor_channel_addr_t addr, uint32_t delay_ms, float bias, float white_noise_density, float random_walk, float temperature_bias, float min_range, float max_range, float resolution);
void hil_sensors_barometer_tick(void*);


/** LIST */

// Barometer's unit is the mBar
#define BAROMETER_1 I2C_CHANNEL, 0x47, 10, 0, 0, 0, 0, -100000000, 100000000, 0
#define BAROMETER_2 I2C_CHANNEL, 0x48, 10, 900, 7, 10, 0, -100000000, 100000000, 6

typedef struct hil_sensors_list_t {
	hil_sensors_barometer_t baro_1;
	hil_sensors_barometer_t baro_2;
} hil_sensors_list_t;

void hil_sensors_init_list(void);
hil_sensors_list_t* hil_sensors_get_list();

#endif
