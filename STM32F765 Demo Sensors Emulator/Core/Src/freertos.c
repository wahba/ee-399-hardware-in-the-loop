/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2024 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "hil_packets.h"
#include "trajectory_memory.h"
#include "sensors.h"
#include "can.h"
#include "stdio.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for debug */
osThreadId_t debugHandle;
const osThreadAttr_t debug_attributes = { .name = "debug",
		.stack_size = 128 * 4, .priority = (osPriority_t) osPriorityNormal, };
/* Definitions for baro_1 */
osThreadId_t baro_1Handle;
const osThreadAttr_t baro_1_attributes = { .name = "baro_1", .stack_size = 256
		* 4, .priority = (osPriority_t) osPriorityHigh, };
/* Definitions for baro_2 */
osThreadId_t baro_2Handle;
const osThreadAttr_t baro_2_attributes = { .name = "baro_2", .stack_size = 256
		* 4, .priority = (osPriority_t) osPriorityNormal, };

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void debug_task(void *argument);
void baro_1_task(void *argument);
void baro_2_task(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
 * @brief  FreeRTOS initialization
 * @param  None
 * @retval None
 */
void MX_FREERTOS_Init(void) {
	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
	/* USER CODE END RTOS_MUTEX */

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
	/* USER CODE END RTOS_TIMERS */

	/* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
	/* USER CODE END RTOS_QUEUES */

	/* Create the thread(s) */
	/* creation of debug */
	debugHandle = osThreadNew(debug_task, NULL, &debug_attributes);

	/* creation of baro_1 */
	baro_1Handle = osThreadNew(baro_1_task, NULL, &baro_1_attributes);

	/* creation of baro_2 */
	baro_2Handle = osThreadNew(baro_2_task, NULL, &baro_2_attributes);

	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */

	/* USER CODE BEGIN RTOS_EVENTS */
	/* add events, ... */
	/* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_debug_task */
/**
 * @brief  Function implementing the debug thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_debug_task */
void debug_task(void *argument) {
	/* USER CODE BEGIN debug_task */
	/* Infinite loop */
	for (;;) {
		get_pressure();
		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
		osDelay(1);
	}
	/* USER CODE END debug_task */
}

/* USER CODE BEGIN Header_baro_1_task */
/**
 * @brief Function implementing the baro_1 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_baro_1_task */
void baro_1_task(void *argument) {
	/* USER CODE BEGIN baro_1_task */
	TickType_t xLastWakeTime = xTaskGetTickCount();

	/* Infinite loop */
	for (;;) {
		hil_sensors_barometer_t baro_1 = hil_sensors_get_list()->baro_1;

		// Updating sensors internal mechanics and reading
		baro_1.base.tick(&baro_1);

		// Write packets to CAN bus
		hil_packet_t packet;
		packet.id = SENSOR_DATA_PACKET;
		packet.sensor_data.uuid = baro_1.base.uuid;
		packet.sensor_data.reg = PRESSURE_1_REGISTER;
		packet.sensor_data.as_float = baro_1.pressure;

		CAN_TxHeaderTypeDef msg;
		msg.StdId = packet.id;
		msg.IDE = CAN_ID_STD;
		msg.RTR = CAN_RTR_DATA;
		msg.DLC = 8;
		uint32_t mailbox = CAN_TX_MAILBOX0;

		HAL_CAN_AddTxMessage(&hcan1, &msg, packet.raw, &mailbox);
		printf("Sent Barometer 1: %.4f\n", packet.sensor_data.as_float);

		// Block task for sensor delay
		// Imitates sampling rate of a real sensor
		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(baro_1.base.delay_ms));
	}
	/* USER CODE END baro_1_task */
}

/* USER CODE BEGIN Header_baro_2_task */
/**
 * @brief Function implementing the baro_2 thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_baro_2_task */
void baro_2_task(void *argument) {
	/* USER CODE BEGIN baro_2_task */
	TickType_t xLastWakeTime = xTaskGetTickCount();

	/* Infinite loop */
	for (;;) {
		hil_sensors_barometer_t baro_2 = hil_sensors_get_list()->baro_2;

		// Updating sensors internal mechanics and reading
		baro_2.base.tick(&baro_2);

		// Write packets to CAN bus
		hil_packet_t packet;
		packet.id = SENSOR_DATA_PACKET;
		packet.sensor_data.uuid = baro_2.base.uuid;
		packet.sensor_data.reg = PRESSURE_2_REGISTER;
		packet.sensor_data.as_float = baro_2.pressure;

		CAN_TxHeaderTypeDef msg;
		msg.StdId = packet.id;
		msg.IDE = CAN_ID_STD;
		msg.RTR = CAN_RTR_DATA;
		msg.DLC = 8;
		uint32_t mailbox = CAN_TX_MAILBOX0;

		HAL_CAN_AddTxMessage(&hcan1, &msg, packet.raw, &mailbox);
		printf("Sent Barometer 2: %.4f\n", packet.sensor_data.as_float);

		// Block task for sensor delay
		// Imitates sampling rate of a real sensor
		vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(baro_2.base.delay_ms));
	}
	/* USER CODE END baro_2_task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

