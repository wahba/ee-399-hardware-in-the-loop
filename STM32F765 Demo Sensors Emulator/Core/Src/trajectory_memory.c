/*
 * trajectory_memory.c
 *
 *  Created on: May 27, 2024
 *      Author: Samuel
 */

#include "trajectory_memory.h"
#include "math.h"
float get_pressure() {
	static float altitude;
	altitude = altitude > 10000 ? 0 : altitude;

	return (1013.25 * pow(1 - ((altitude++) / 44307.692), 1.0 / 0.190284));
}
