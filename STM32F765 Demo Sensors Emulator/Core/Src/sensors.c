/*
 * sensors.c
 *
 *  Created on: May 27, 2024
 *      Author: Samuel
 */

#include "sensors.h"
#include "sensor_models.h"
#include "trajectory_memory.h"

hil_sensors_list_t LIST = {};

const sensor_uuid_t get_sensor_uuid(uint32_t addr, sensors_channels_t channel) {
	return ((uint16_t) channel) << 8 | ((uint16_t) addr);
}

void hil_sensors_new_barometer(hil_sensors_barometer_t* baro, sensors_channels_t channel, sensor_channel_addr_t addr, uint32_t delay_ms, float bias, float white_noise_density, float random_walk, float temperature_bias, float min_range, float max_range, float resolution) {
	baro->base.channel = channel;
	baro->base.addr = addr;
	baro->base.uuid = get_sensor_uuid(addr, channel);
	baro->base.delay_ms = delay_ms;
	baro->base.sampling_rate = 1./(double) delay_ms;

	baro->base.tick = hil_sensors_barometer_tick;

	baro->pressure = 0;
	baro->bias = bias;
	baro->white_noise_density = white_noise_density;
	baro->random_walk = random_walk;
	baro->temperature_bias = temperature_bias;
	baro->min_range = min_range;
	baro->max_range = max_range;
	baro->resolution = resolution;
}

void hil_sensors_barometer_tick(void* baro){
	hil_sensors_barometer_t* as_baro = (hil_sensors_barometer_t*) baro;
	as_baro->pressure = get_pressure();
	bulk_model(&(as_baro->pressure), &(as_baro->bias));
	white_noise_drift_model(&(as_baro->pressure), &(as_baro->white_noise_density), &as_baro->base.sampling_rate);
	random_walk_drift_model(&(as_baro->pressure), &(as_baro->random_walk), &as_baro->base.sampling_rate);
	saturation_model(&(as_baro->pressure), &(as_baro->min_range), &(as_baro->max_range));
	quantization_model(&(as_baro->pressure), &(as_baro->resolution));
}

void hil_sensors_init_list() {
	hil_sensors_new_barometer(&(LIST.baro_1), BAROMETER_1);
	hil_sensors_new_barometer(&(LIST.baro_2), BAROMETER_2);
}

hil_sensors_list_t* hil_sensors_get_list() {
	return &LIST;
}
