#include <Arduino.h>
#include <Wire.h>

#define ADDR_1 0x47
#define OFFSET_1 1
#define BUFFER_1 6
uint8_t buffer_1[BUFFER_1];

#define ADDR_2 0x48
#define OFFSET_2 3
#define BUFFER_2 2
uint8_t buffer_2[BUFFER_2];

#define ADDR_3 0x49
#define OFFSET_3 8
#define BUFFER_3 3
uint8_t buffer_3[BUFFER_3];

void write_then_read(uint8_t addr, uint8_t* write_buffer, uint32_t write_buffer_size, uint8_t* read_buffer, uint32_t read_buffer_size, bool stop = false){
  Wire.beginTransmission(addr);
  Wire.write(write_buffer, write_buffer_size);
  Wire.endTransmission((uint8_t)stop);
  
  Wire.requestFrom(addr, read_buffer_size);

  for(uint32_t i = 0; i < read_buffer_size; i++){
    while(!Wire.available());
    read_buffer[i] = Wire.read();
  }

  Wire.endTransmission();

}

void setup() {
  Wire.begin();
  Serial.begin(9600);
}

void test(uint8_t addr, uint8_t offset, uint8_t* buffer, uint8_t buffer_size) {
  Serial.println("=================");
  Serial.printf("Requesting %d bytes from addr %x, starting at idx %d\n", buffer_size, addr, offset);
  write_then_read(ADDR_1, &offset, 1, buffer, buffer_size, false);

  Serial.print("Received: ");
  for(int i = 0; i < buffer_size; i++){
    Serial.printf("%x ", buffer[i]);
  }
  Serial.println();
}

void loop() {
  //Expected data: 0x01 0x02 0x03 0x04 0x05 0x06
  test(ADDR_1, OFFSET_1, buffer_1, BUFFER_1);
  delay(10);
  
  //Expected data: 0x03 0x04 0x05
  test(ADDR_2, OFFSET_2, buffer_2, BUFFER_2);
  delay(10);

  //Expected data: 0x08 0x09 0x00
  test(ADDR_3, OFFSET_3, buffer_3, BUFFER_3);
  delay(1000);
}