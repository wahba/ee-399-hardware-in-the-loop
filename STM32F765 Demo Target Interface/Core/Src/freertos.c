/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2024 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "can.h"
#include "sensors_memory.h"
#include "hil_packets.h"
#include "stdio.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for debug */
osThreadId_t debugHandle;
const osThreadAttr_t debug_attributes = { .name = "debug",
		.stack_size = 128 * 4, .priority = (osPriority_t) osPriorityNormal, };
/* Definitions for can_hanlder */
osThreadId_t can_hanlderHandle;
const osThreadAttr_t can_hanlder_attributes = { .name = "can_hanlder",
		.stack_size = 256 * 4, .priority = (osPriority_t) osPriorityHigh, };

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void debug_task(void *argument);
void can_hanlder_task(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
 * @brief  FreeRTOS initialization
 * @param  None
 * @retval None
 */
void MX_FREERTOS_Init(void) {
	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* USER CODE BEGIN RTOS_MUTEX */
	/* add mutexes, ... */
	/* USER CODE END RTOS_MUTEX */

	/* USER CODE BEGIN RTOS_SEMAPHORES */
	/* add semaphores, ... */
	/* USER CODE END RTOS_SEMAPHORES */

	/* USER CODE BEGIN RTOS_TIMERS */
	/* start timers, add new ones, ... */
	/* USER CODE END RTOS_TIMERS */

	/* USER CODE BEGIN RTOS_QUEUES */
	/* add queues, ... */
	/* USER CODE END RTOS_QUEUES */

	/* Create the thread(s) */
	/* creation of debug */
	debugHandle = osThreadNew(debug_task, NULL, &debug_attributes);

	/* creation of can_hanlder */
	can_hanlderHandle = osThreadNew(can_hanlder_task, NULL,
			&can_hanlder_attributes);

	/* USER CODE BEGIN RTOS_THREADS */
	/* add threads, ... */
	/* USER CODE END RTOS_THREADS */

	/* USER CODE BEGIN RTOS_EVENTS */
	/* add events, ... */
	/* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_debug_task */
/**
 * @brief  Function implementing the debug thread.
 * @param  argument: Not used
 * @retval None
 */
/* USER CODE END Header_debug_task */
void debug_task(void *argument) {
	/* USER CODE BEGIN debug_task */
	/* Infinite loop */
	for (;;) {
		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
		osDelay(1);
	}
	/* USER CODE END debug_task */
}

/* USER CODE BEGIN Header_can_hanlder_task */
/**
 * @brief Function implementing the can_hanlder thread.
 * @param argument: Not used
 * @retval None
 */
/* USER CODE END Header_can_hanlder_task */
void can_hanlder_task(void *argument) {
	/* USER CODE BEGIN can_hanlder_task */
	/* Infinite loop */
	for (;;) {
		if (HAL_CAN_GetRxFifoFillLevel(&hcan1, CAN_RX_FIFO0) > 0) {

			CAN_RxHeaderTypeDef header;
			hil_packet_t packet;

			// Read packet
			HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &header, packet.raw);
			packet.id = (hil_packets_names_t) header.StdId;
			hil_sensors_registers_name_t reg = INVALID_REGISTER;

			// Update register values
			switch (packet.id) {
			case SENSOR_DATA_PACKET:
				reg = map_register_from_uuid(packet.sensor_data.uuid);
				set_sensors_register_value(reg, packet.sensor_data.as_u32);
				break;
			default:
				break;
			}
		}

		// xSemaphoreGive(sensors_memory_mutexHandle);
		osDelay(1);
	}
	/* USER CODE END can_hanlder_task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

