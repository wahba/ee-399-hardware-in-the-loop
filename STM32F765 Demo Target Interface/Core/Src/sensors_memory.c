/*
 * sensors_emulator_interface.c
 *
 *  Created on: May 27, 2024
 *      Author: Samuel
 */

#include <sensors_memory.h>

hil_sensors_registers_t REGISTERS = {};

void init_sensors_registers() {
	REGISTERS.pressure_1.value = 0;
	REGISTERS.pressure_2.value = 0;
}

const sensor_uuid_t get_sensor_uuid(uint32_t addr, sensors_channels_t channel) {
	return ((uint16_t) channel) << 8 | ((uint16_t) addr);
}

hil_sensors_registers_name_t map_register_from_uuid(sensor_uuid_t uuid) {
	if(uuid == get_sensor_uuid(BAROMETER_1))
			return PRESSURE_1_REGISTER;
	if(uuid == get_sensor_uuid(BAROMETER_2))
			return PRESSURE_2_REGISTER;

	return INVALID_REGISTER;
}

hil_sensors_registers_name_t map_register(uint32_t addr, sensors_channels_t channel) {
	return map_register_from_uuid(get_sensor_uuid(addr, channel));
}

uint8_t get_sensors_register_byte(hil_sensors_registers_name_t reg, uint8_t byte) {
	switch (reg) {
	case PRESSURE_1_REGISTER:
		return REGISTERS.pressure_1.raw[byte];
	case PRESSURE_2_REGISTER:
		return REGISTERS.pressure_2.raw[byte];
	case INVALID_REGISTER:
	default:
		return 0;
	}
}

uint32_t get_sensors_register_value(hil_sensors_registers_name_t reg) {
	switch (reg) {
	case PRESSURE_1_REGISTER:
		return REGISTERS.pressure_1.as_u32;
	case PRESSURE_2_REGISTER:
		return REGISTERS.pressure_2.as_u32;
	case INVALID_REGISTER:
	default:
		return 0;
	}
}


void set_sensors_register_value(hil_sensors_registers_name_t reg, uint32_t value) {
	switch (reg) {
	case PRESSURE_1_REGISTER:
		REGISTERS.pressure_1.as_u32 = value;
		return;
	case PRESSURE_2_REGISTER:
		REGISTERS.pressure_2.as_u32 = value;
		return;
	case INVALID_REGISTER:
	default:
		return;
	}
}

uint8_t get_sensors_registers_size(hil_sensors_registers_name_t reg) {
	switch (reg) {
	case PRESSURE_1_REGISTER:
		return sizeof(REGISTERS.pressure_1.value);
	case PRESSURE_2_REGISTER:
			return sizeof(REGISTERS.pressure_2.value);
	case INVALID_REGISTER:
	default:
		return 0;
	}
}
