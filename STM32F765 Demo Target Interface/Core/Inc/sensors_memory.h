/*
 * sensors_emulator.h
 *
 *  Created on: May 27, 2024
 *      Author: Samuel
 */

#ifndef INC_SENSORS_MEMORY_H_
#define INC_SENSORS_MEMORY_H_

#include "stdint.h"

/**
 * Sensors Channels as defined in the emulator.
 */
typedef enum sensors_channels_t {
	INVALID_CHANNEL = 0, I2C_CHANNEL = 1,
} sensors_channels_t;

/**
 * Sensors UUID as defined in the emulator.
 */
typedef uint16_t sensor_uuid_t;

#define BAROMETER_1_ADDR 0x47
#define BAROMETER_1_CHANNEL I2C_CHANNEL
#define BAROMETER_1 BAROMETER_1_ADDR, BAROMETER_1_CHANNEL

#define BAROMETER_2_ADDR 0x48
#define BAROMETER_2_CHANNEL I2C_CHANNEL
#define BAROMETER_2 BAROMETER_2_ADDR, BAROMETER_2_CHANNEL

typedef union hil_barometer_register_t {
	float value;
	uint32_t as_u32;
	uint8_t raw[sizeof(float)];
} hil_barometer_register_t;

/**
 * A memory space dedicated to storing the values generated by the emulator.
 * Acts as a cache between the target and the emulator part of the HIL.
 */
typedef struct hil_sensors_registers_t {
	hil_barometer_register_t pressure_1;
	hil_barometer_register_t pressure_2;
} hil_sensors_registers_t;

/**
 * A memory map for the sensors' register bank
 */
typedef enum hil_sensors_registers_name_t {
	INVALID_REGISTER = 0, PRESSURE_1_REGISTER = 1, PRESSURE_2_REGISTER = 2,
} hil_sensors_registers_name_t;

void init_sensors_registers(void);

const sensor_uuid_t get_sensor_uuid(uint32_t addr, sensors_channels_t channel);

hil_sensors_registers_name_t map_register_from_uuid(sensor_uuid_t uuid);
hil_sensors_registers_name_t map_register(uint32_t addr,
		sensors_channels_t channel);

uint8_t get_sensors_register_byte(hil_sensors_registers_name_t reg,
		uint8_t byte);
uint32_t get_sensors_register_value(hil_sensors_registers_name_t reg);

void set_sensors_register_value(hil_sensors_registers_name_t reg,
		uint32_t value);

uint8_t get_sensors_registers_size(hil_sensors_registers_name_t reg);

#endif /* INC_SENSORS_MEMORY_H_ */
