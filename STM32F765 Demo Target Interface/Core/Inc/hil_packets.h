/*
 * hil_packets.h
 *
 *  Created on: May 27, 2024
 *      Author: Samuel
 */

#ifndef INC_HIL_PACKETS_H_
#define INC_HIL_PACKETS_H_

#include "sensors_memory.h"

typedef enum hil_packets_names_t {
	INVALID_PACKET = 0,
	SENSOR_DATA_PACKET = 1,
} hil_packets_names_t;

typedef struct hil_packet_t {
	hil_packets_names_t id;
	union {
		uint8_t raw[8]; // Standard Size for all packets

		struct {
			sensor_uuid_t uuid;
			union {
				uint32_t as_u32;
				int32_t as_i32;
				float as_float;
			};

			uint8_t reserved[2];

		} sensor_data;
	};
} hil_packet_t;

#endif /* INC_HIL_PACKETS_H_ */
